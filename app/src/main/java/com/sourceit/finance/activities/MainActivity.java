package com.sourceit.finance.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.sourceit.finance.R;
import com.sourceit.finance.fragments.CreditFragment;
import com.sourceit.finance.fragments.DepositFragment;
import com.sourceit.finance.model.Credit;
import com.sourceit.finance.model.Deposit;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity{

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        unbinder = ButterKnife.bind(this);
        callCreditFragment();
    }

    @OnClick(R.id.credit_btn)
    public void callCreditFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CreditFragment.newInstance())
                .commit();
    }

    @OnClick(R.id.deposit_btn)
    public void callDepositFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, DepositFragment.newInstance())
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
