package com.sourceit.finance.model;

public class Credit {
    public float sum;
    private float percent;
    public float month;
    private float insurance;
    private float pre;
    private float tax;

    public Credit(float sum, float percent, float month, float insurance, float pre, float tax) {
        this.sum = sum;
        setPercent(percent);
        this.month = month;
        setInsurance(insurance);
        setPre(pre);
        setTax(tax);
    }

    public float getInitialFee() {
        return sum * pre;
    }

    public float getMonthPayment() {
        if (month != 0) {
            return (sum / month) * percent * insurance * tax;
        }
        return 0;
    }

    public float getOverPayment() {
        return sum * percent * insurance * tax - sum;
    }

    public float getTotalSum() {
        return sum * percent * insurance * tax;
    }

    public void setPre(float pre) {
        this.pre = pre / 100;
    }

    public void setPercent(float percent) {
        this.percent = 1 + percent / 100;
    }

    public void setInsurance(float insurance) {
        this.insurance = 1 + insurance / 100;
    }

    public void setTax(float tax) {
        this.tax = 1 + tax / 100;
    }
}
