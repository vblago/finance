package com.sourceit.finance.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.finance.OnTextChangeListener;
import com.sourceit.finance.R;
import com.sourceit.finance.model.Credit;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditFragment extends Fragment {

    @BindView(R.id.sum_credit_edit) EditText sumCreditEdit;
    @BindView(R.id.percent_credit_edit) EditText percentCreditEdit;
    @BindView(R.id.month_credit_edit) EditText monthCreditEdit;
    @BindView(R.id.insurance_credit_edit) EditText insuranceCreditEdit;
    @BindView(R.id.pre_credit_edit) EditText preCreditEdit;
    @BindView(R.id.tax_credit_edit) EditText taxCreditEdit;

    @BindView(R.id.initial_fee) TextView initialFeeView;
    @BindView(R.id.month_payment) TextView monthPaymentView;
    @BindView(R.id.overpayment) TextView overpaymentView;
    @BindView(R.id.total_sum) TextView totalSumView;

    Credit credit;

    View root;

    public static CreditFragment newInstance() {
        return new CreditFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_credit, container, false);
        ButterKnife.bind(this, root);

        credit = new Credit(0, 0, 0, 0, 0, 0);

        sumCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.sum = (!sumCreditEdit.getText().toString().equals("")) ? Float.valueOf(sumCreditEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        percentCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setPercent((!percentCreditEdit.getText().toString().equals("")) ? Float.valueOf(percentCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        monthCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.month = (!monthCreditEdit.getText().toString().equals("")) ? Float.valueOf(monthCreditEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        insuranceCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setInsurance((!insuranceCreditEdit.getText().toString().equals("")) ? Float.valueOf(insuranceCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        preCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setPre((!preCreditEdit.getText().toString().equals("")) ? Float.valueOf(preCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        taxCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setTax((!taxCreditEdit.getText().toString().equals("")) ? Float.valueOf(taxCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });
        return root;
    }

//    @OnTextChanged({ R.id.sum_credit_edit, R.id.percent_credit_edit})
//    public void pickDoor(View editText) {
//        if (editText.equals(sumCreditEdit)) {
//            Log.i("hello", "sum");
//        } else if (editText.equals(percentCreditEdit)){
//            Log.i("hello", "percent");
//        }
//    }

    private void showChanges() {
        initialFeeView.setText(String.format(Locale.US, "%.2f",  credit.getInitialFee()));
        monthPaymentView.setText(String.format(Locale.US, "%.2f", credit.getMonthPayment()));
        overpaymentView.setText(String.format(Locale.US, "%.2f", credit.getOverPayment()));
        totalSumView.setText(String.format(Locale.US, "%.2f", credit.getTotalSum()));
    }
}
