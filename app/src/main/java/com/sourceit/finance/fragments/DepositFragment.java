package com.sourceit.finance.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.finance.OnTextChangeListener;
import com.sourceit.finance.R;
import com.sourceit.finance.model.Deposit;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DepositFragment extends Fragment {

    @BindView(R.id.sum_edit) EditText sumEdit;
    @BindView(R.id.percent_edit) EditText percentEdit;
    @BindView(R.id.month_edit) EditText monthEdit;
    @BindView(R.id.add_edit) EditText addEdit;
    @BindView(R.id.tax_edit) EditText taxEdit;

    @BindView(R.id.month_income) TextView monthIncomeView;
    @BindView(R.id.total_income) TextView totalIncomeView;
    @BindView(R.id.total_month_add) TextView totalAddView;
    @BindView(R.id.total) TextView totalSumView;

    @BindView(R.id.capital)
    CheckBox capitalCheckView;

    Deposit dep;

    public static DepositFragment newInstance() {
        return new DepositFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_deposit, container, false);
        ButterKnife.bind(this, root);

        dep = new Deposit(0, 0, 0, 0, 0, false);

        sumEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                dep.sum = (!sumEdit.getText().toString().equals("")) ? Float.valueOf(sumEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        percentEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                dep.setPercent((!percentEdit.getText().toString().equals("")) ? Float.valueOf(percentEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        monthEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                dep.month = (!monthEdit.getText().toString().equals("")) ? Float.valueOf(monthEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        addEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                dep.add = (!addEdit.getText().toString().equals("")) ? Float.valueOf(addEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        taxEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                dep.setTax((!taxEdit.getText().toString().equals("")) ? Float.valueOf(taxEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        capitalCheckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dep.capital = isChecked;
                showChanges();
            }
        });

        return root;
    }

    private void showChanges() {
        monthIncomeView.setText(String.format(Locale.US, "%.2f", dep.getMonthIncome()));
        totalIncomeView.setText(String.format(Locale.US, "%.2f", dep.getTotalIncome()));
        totalAddView.setText(String.format(Locale.US, "%.2f", dep.getTotalAdd()));
        totalSumView.setText(String.format(Locale.US, "%.2f", dep.getTotalSum()));
    }

}
